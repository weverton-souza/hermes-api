#  Hermes API

## Testes
#### Tecnologias necessárias:
* ##### Gradle, Docker, Docker Compose.

<br />

#### Comandos:

* ##### docker-compose -f docker-compose.test.yml up
* ##### ./gradlew clean test --info 

<br />
<br />

## Iniciar a aplicação
#### Tecnologias necessárias:
* ##### Gradle, Docker, Docker Compose.

<br />

#### Comandos:
* ##### ./gradlew bootWar
* ##### docker-compose -f docker-compose.dev.yml up

<br />

## Acessos à documentação da aplicação

### Ambiente de Desenvolvimento
* ###[Acesso ao Swagger](http://localhost:8080/swagger-ui.html)
* ###[Acesso ao PGAdmin](http://localhost:5050/)

### Ambiente de Testes
* ###[Acesso ao PGAdmin](http://localhost:5051/)
<br />
