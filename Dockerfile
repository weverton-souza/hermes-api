# Build Stage for Spring boot application image
FROM openjdk:11-jdk-slim

ENV APP_TARGET /build/libs
ENV APP hermes-api-0.0.1.war

RUN mkdir -p /environment
COPY ${APP_TARGET}/${APP} /environment
EXPOSE 8080
