package com.improving.facade

import com.improving.exception.ResourceNotFoundException
import com.improving.repository.AssigneeRepository
import com.improving.repository.CreatorRepository
import com.improving.repository.IssueBaseRepository
import com.improving.repository.IssueRepository
import com.improving.repository.LabelRepository
import com.improving.repository.MilestoneRepository
import com.improving.repository.OwnerRepository
import com.improving.repository.RepoGitRepository
import com.improving.repository.SenderRepository
import com.improving.repository.UserRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class IssueEventFacadeTest {

    private var issueBaseRepository = Mockito.mock(IssueBaseRepository::class.java)
    private var issueRepository = Mockito.mock(IssueRepository::class.java)
    private var ownerRepository = Mockito.mock(OwnerRepository::class.java)
    private var repoGitRepository = Mockito.mock(RepoGitRepository::class.java)
    private var senderRepository = Mockito.mock(SenderRepository::class.java)
    private var userRepository = Mockito.mock(UserRepository::class.java)
    private var assigneeRepository = Mockito.mock(AssigneeRepository::class.java)
    private var milestoneRepository = Mockito.mock(MilestoneRepository::class.java)
    private var creatorRepository = Mockito.mock(CreatorRepository::class.java)
    private var labelRepository = Mockito.mock(LabelRepository::class.java)

    val issueEventFacade = IssueEventFacade(
            issueBaseRepository,
            issueRepository,
            ownerRepository,
            repoGitRepository,
            senderRepository,
            userRepository,
            assigneeRepository,
            milestoneRepository,
            creatorRepository,
            labelRepository
    )

    @Test
    fun `Find Issue By External Id, should be return ResourceNotFoundException`() {
        Assertions.assertThrows(ResourceNotFoundException::class.java) {
            this.issueEventFacade.findIssueByExternalId(123)
        }
    }

    @Test
    fun `Find Event By Issue External Id, should be return ResourceNotFoundException`() {
        Assertions.assertThrows(ResourceNotFoundException::class.java) {
            this.issueEventFacade.findEventByIssueExternalId(123)
        }
    }
}
