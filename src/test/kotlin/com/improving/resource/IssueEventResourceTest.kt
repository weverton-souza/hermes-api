package com.improving.resource

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.improving.domain.events.IssueEvent
import com.improving.service.IssueEventService
import java.io.File
import java.nio.file.Paths
import org.junit.Assert
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@SpringBootTest
class IssueEventResourceTest{

    @Autowired
    lateinit var webApplicationContext: WebApplicationContext

    @Autowired
    lateinit var issueEventService: IssueEventService

    lateinit var mockMvc: MockMvc

    lateinit var absolutePath: String

    private val objectMapper = ObjectMapper().registerModule(KotlinModule())


    @BeforeEach
    @Throws(Exception::class)
    fun setup() {
        this.absolutePath = Paths
                .get("src", "test", "resources")
                .toFile().absolutePath + "/data-json/resource/issue-event-resource/"
        mockMvc = this.webApplicationContext.let { MockMvcBuilders.webAppContextSetup(it).build() }
    }

    @Test
    fun `Create Issue`() {
        val issueEvent = this.objectMapper
                .readValue(File("$absolutePath/create-a-issue-event.json"), IssueEvent::class.java)

        val issueEventSaved =  issueEventService.save(issueEvent)

       Assert.assertNotNull(issueEventSaved)
    }

    @Test
    fun `Find By Issue External Id`() {
        val issueEvent = this.objectMapper
                .readValue(File("$absolutePath/find-by-Issue-external-id.json"), IssueEvent::class.java)

        issueEventService.save(issueEvent)

        val issueEventRetrieved = issueEventService.findEventByIssueExternalId(444500042)

        Assert.assertNotNull(issueEvent)
        Assert.assertNotNull(issueEventRetrieved)
        Assert.assertEquals(issueEvent.issue.externalId, issueEventRetrieved.issue.externalId)
    }
}
