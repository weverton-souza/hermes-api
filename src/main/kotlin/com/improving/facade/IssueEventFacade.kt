package com.improving.facade

import com.improving.domain.Issue
import com.improving.domain.events.IssueEvent
import com.improving.exception.ResourceNotFoundException
import com.improving.repository.AssigneeRepository
import com.improving.repository.CreatorRepository
import com.improving.repository.IssueBaseRepository
import com.improving.repository.IssueRepository
import com.improving.repository.LabelRepository
import com.improving.repository.MilestoneRepository
import com.improving.repository.OwnerRepository
import com.improving.repository.RepoGitRepository
import com.improving.repository.SenderRepository
import com.improving.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class IssueEventFacade(
        private val issueBaseRepository: IssueBaseRepository,
        private val issueRepository: IssueRepository,
        private val ownerRepository: OwnerRepository,
        private val repoGitRepository: RepoGitRepository,
        private val senderRepository: SenderRepository,
        private val userRepository: UserRepository,
        private val assigneeRepository: AssigneeRepository,
        private val milestoneRepository: MilestoneRepository,
        private val creatorRepository: CreatorRepository,
        private val labelRepository: LabelRepository
) {

    fun saveIssue(issueEvent: IssueEvent): IssueEvent {

        issueEvent.issue.user = this.userRepository.save(issueEvent.issue.user)
        issueEvent.sender = this.senderRepository.save(issueEvent.sender)
        issueEvent.repository.owner = this.ownerRepository.save(issueEvent.repository.owner)
        issueEvent.repository = this.repoGitRepository.save(issueEvent.repository)

        issueEvent.issue = this.issueRepository.save(issueEvent.issue)

        issueEvent.issue.labels = issueEvent.issue.labels?.let { this.labelRepository.saveAll(it) }
        issueEvent.issue.assignees = issueEvent.issue.assignees?.let { this.assigneeRepository.saveAll(it) }
        issueEvent.issue.assignee = issueEvent.issue.assignee?.let { this.assigneeRepository.save(it) }
        issueEvent.issue.milestone?.creator  = issueEvent.issue.milestone?.creator?.let { this.creatorRepository.save(it) }!!
        issueEvent.issue.milestone = issueEvent.issue.milestone?.let { this.milestoneRepository.save(it) }


        return this.issueBaseRepository.save(issueEvent)
    }

    fun findIssueByExternalId(issueId: Long): Issue {
        return this.issueRepository.findByExternalId(issueId)
                .orElseThrow { ResourceNotFoundException() }
    }

    fun findEventByIssueExternalId(issueId: Long): IssueEvent {
        val issue = this.findIssueByExternalId(issueId)
        return issueBaseRepository.findById(issue.issueId!!)
                .orElseThrow { ResourceNotFoundException() }
    }

}
