package com.improving.configuration.swagger

import com.google.common.base.Predicates
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ResourceLoader
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
@Suppress("UnstableApiUsage")
class SwaggerConfiguration(val resourceLoader: ResourceLoader) {

    @Bean
    fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.improving.resource"))
            .paths(Predicates.not(PathSelectors.regex("/error")))
            .build()
            .apiInfo(metadata())

    private fun metadata(): ApiInfo =
        ApiInfoBuilder()
                .title("Hermes API")
                .description("")
                .version("1.0")
                .build()
}