package com.improving.domain

import com.fasterxml.jackson.annotation.JsonAlias
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table


@Entity
@Table(name = "label_git")
class Label(
        @Id @Column(name = "label_id") @GeneratedValue(strategy = GenerationType.AUTO) var labelId: Long?,
        @JoinColumn(name = "issue_id") @ManyToOne var issue: Issue?,
        var id: Long,
        @JsonAlias("node_id") @Column(name = "node_id") var node_id: String,
        @Column(name = "url") var url: String,
        @Column(name = "name") var name: String,
        @Column(name = "color") var color: String,
        @Column(name = "default_label") var default: String
) : Serializable
