package com.improving.domain.events

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonIgnore
import com.improving.domain.Issue
import com.improving.domain.Repository
import com.improving.domain.Sender
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table


@Entity
@Table(name = "issue_git")
class IssueEvent(
        @Id
        @Column(name = "issue_id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        var issueBaseId: Long?,

        val action: String,

        @JoinColumn(name = "issue_id")
        @OneToOne
        var issue: Issue,

        @JoinColumn(name = "repository_id")
        @OneToOne
        var repository: Repository,

        @JoinColumn(name = "sender_id")
        @OneToOne
        var sender: Sender

) : Serializable

