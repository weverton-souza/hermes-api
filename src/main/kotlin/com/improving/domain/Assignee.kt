package com.improving.domain

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table


@Entity
@Table(name = "assignee_git")
data class Assignee(
        @Id @JsonIgnore @Column(name = "assignee_id") @GeneratedValue(strategy=GenerationType.AUTO)var AssigneeId: Long?,
        @JoinColumn(name = "issue_assignee_id") @OneToOne var issueAssignee: Issue?,
        @JoinColumn(name = "issue_assignees_id") @ManyToOne var issueAssignees: Issue?,
        @Column(name = "login") var login: String,
        @Column(name = "id") var id: Int,
        @JsonAlias("node_id") @Column(name = "node_id") var nodeId: String,
        @JsonAlias("avatar_url") @Column(name = "avatar_url") var avatarUrl: String,
        @JsonAlias("gravatar_id") @Column(name = "gravatar_id") var gravatarId: String,
        @Column(name = "url") var url: String,
        @JsonAlias("html_url") @Column(name = "html_url") var htmlUrl: String,
        @JsonAlias("followers_url") @Column(name = "followers_url") var followersUrl: String,
        @JsonAlias("following_url") @Column(name = "following_url") var followingUrl: String,
        @JsonAlias("gists_url") @Column(name = "gists_url") var gistsUrl: String,
        @JsonAlias("starred_url") @Column(name = "starred_url") var starredUrl: String,
        @JsonAlias("subscriptions_url") @Column(name = "subscriptions_url") var subscriptionsUrl: String,
        @JsonAlias("organizations_url") @Column(name = "organizations_url") var organizationsUrl: String,
        @JsonAlias("repos_url") @Column(name = "repos_url") var reposUrl: String,
        @JsonAlias("events_url") @Column(name = "events_url") var eventsUrl: String,
        @JsonAlias("received_events_url") @Column(name = "received_events_url") var receivedEventsUrl: String,
        @Column(name = "type") var type: String,
        @JsonAlias("site_admin") @Column(name = "site_admin") var siteAdmin: Boolean
) : Serializable
