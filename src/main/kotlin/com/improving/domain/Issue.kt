package com.improving.domain

import com.fasterxml.jackson.annotation.JsonAlias
import java.io.Serializable
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table


@Entity
@Table(name = "issue_git")
class Issue(
        @Id @Column(name = "issue_id") @GeneratedValue(strategy=GenerationType.AUTO) var issueId: Long?,

        @Column(name = "url") var url: String?,
        @JsonAlias("repository_url") @Column(name = "repository_url") var repositoryUrl: String?,
        @JsonAlias("labels_url") @Column(name = "labels_url") var labelsUrl: String?,
        @JsonAlias("comments_url") @Column(name = "comments_url") var commentsUrl: String?,
        @JsonAlias("events_url") @Column(name = "events_url") var eventsUrl: String?,
        @JsonAlias("html_url") @Column(name = "html_url") var htmlUrl: String?,
        @JsonAlias("id") @Column(name = "id") var externalId: Long?,
        @JsonAlias("node_id") @Column(name = "node_id") var nodeId: String?,
        @Column(name = "number") var number: Int?,
        @Column(name = "title") var title: String?,
        @JsonAlias("user_id") @JoinColumn(name = "user_id") @OneToOne var user: User,
        @JsonAlias("labels") @OneToMany(cascade = [CascadeType.ALL], mappedBy = "issue") var labels: List<Label>?,
        @Column(name = "state") var state: String?,
        @Column(name = "locked") var locked: Boolean?,
        @OneToOne(cascade= [CascadeType.ALL], mappedBy = "issueAssignee") var assignee: Assignee?,
        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "issueAssignees") var assignees: List<Assignee>?,
        @JoinColumn(name = "milestone") @OneToOne(cascade= [CascadeType.ALL]) var milestone: Milestone?,
        @Column(name = "comments") var comments: Int?,
        @JsonAlias("created_at") @Column(name = "created_at") var createdAt: String?,
        @JsonAlias("updated_at") @Column(name = "updated_at") var updatedAt: String?,
        @JsonAlias("closed_at") @Column(name = "closed_at") var closedAt: String?,
        @JsonAlias("author_association") @Column(name = "author_association") var authorAssociation: String?,
        @JsonAlias("active_lock_reason") @Column(name = "active_lock_reason") var activeLockReason: String?,
        @Column(name = "body") var body: String?,
        @JsonAlias("performed_via_github_app") @Column(name = "performed_via_github_app") var performedViaGithubApp: String?
) : Serializable