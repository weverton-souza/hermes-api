package com.improving.domain

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "milestone_git")
data class Milestone(
        @Id @Column(name = "milestone_id") @GeneratedValue(strategy = GenerationType.AUTO) var milestoneId: Long?,
        @Column(name = "url") var url: String,
        @JsonAlias("html_url") @Column(name = "html_url") var html_url: String,
        @JsonAlias("labels_url") @Column(name = "labels_url") var labels_url: String,
        @Column(name = "id") var id: Int,
        @JsonAlias("node_id") @Column(name = "node_id") var node_id: String,
        @Column(name = "number") var number: Int,
        @Column(name = "title") var title: String,
        @Column(name = "description") var description: String,
        @Column(name = "creator") var creator: Creator,
        @JsonAlias("open_issues") @Column(name = "open_issues") var open_issues: Int,
        @JsonAlias("closed_issues") @Column(name = "closed_issues") var closed_issues: Int,
        @Column(name = "state") var state: String,
        @JsonAlias("created_at") @Column(name = "created_at") var created_at: String,
        @JsonAlias("updated_at") @Column(name = "updated_at") var updated_at: String,
        @JsonAlias("due_on") @Column(name = "due_on") var due_on: String,
        @JsonAlias("closed_at") @Column(name = "closed_at") var closed_at: String
) : Serializable
