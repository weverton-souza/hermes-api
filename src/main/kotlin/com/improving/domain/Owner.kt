package com.improving.domain

import com.fasterxml.jackson.annotation.JsonAlias
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "owner_git")
data class Owner(
        @Column(name = "owner_id") @Id @GeneratedValue(strategy = GenerationType.AUTO) val ownerId: Long?,
        @Column(name = "login") val login: String?,
        @Column(name = "id") val id: Int?,
        @JsonAlias("node_id") @Column(name = "node_id") val nodeId: String?,
        @JsonAlias("avatar_url") @Column(name = "avatar_url") val avatarUrl: String?,
        @JsonAlias("gravatar_id") @Column(name = "gravatar_id") val gravatarId: String?,
        @Column(name = "url") val url: String?,
        @JsonAlias("html_url") @Column(name = "html_url") val htmlUrl: String?,
        @JsonAlias("followers_url") @Column(name = "followers_url") val followersUrl: String?,
        @JsonAlias("following_url") @Column(name = "following_url") val followingUrl: String?,
        @JsonAlias("gists_url") @Column(name = "gists_url") val gistsUrl: String?,
        @JsonAlias("starred_url") @Column(name = "starred_url") val starredUrl: String?,
        @JsonAlias("subscriptions_url") @Column(name = "subscriptions_url") val subscriptionsUrl: String?,
        @JsonAlias("organizations_url") @Column(name = "organizations_url") val organizationsUrl: String?,
        @JsonAlias("repos_url") @Column(name = "repos_url") val reposUrl: String?,
        @JsonAlias("events_url") @Column(name = "events_url") val eventsUrl: String?,
        @JsonAlias("received_events_url") @Column(name = "received_events_url") val receivedEventsUrl: String?,
        @Column(name = "type") val type: String?,
        @JsonAlias("site_admin") @Column(name = "site_admin") val siteAdmin: Boolean?
) : Serializable
