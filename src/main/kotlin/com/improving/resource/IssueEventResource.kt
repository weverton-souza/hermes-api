package com.improving.resource

import com.improving.domain.events.IssueEvent
import com.improving.service.IssueEventService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/events")
@Api(value = "IssueEvents", tags = [":: ISSUE EVENTS ::"], description = "Issue events resources")
class IssueEventResource(
        private val issueEventService: IssueEventService
) {

    @PostMapping
    @ApiOperation(value = "Save a issue event")
    fun createIssue(@RequestBody issueEvent: IssueEvent) = this.issueEventService.save(issueEvent)

    @GetMapping("/issues/{externalId}")
    @ApiOperation(value = "Retrieve issue by external id")
    fun findByIssueExternalId(@PathVariable externalId: Long) =
            this.issueEventService.findEventByIssueExternalId(externalId)
}
