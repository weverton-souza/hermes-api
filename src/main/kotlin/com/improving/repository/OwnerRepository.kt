package com.improving.repository

import com.improving.domain.Owner
import org.springframework.data.jpa.repository.JpaRepository

interface OwnerRepository: JpaRepository<Owner, Long>
