package com.improving.repository

import com.improving.domain.events.IssueEvent
import org.springframework.data.jpa.repository.JpaRepository

interface IssueBaseRepository: JpaRepository<IssueEvent, Long>
