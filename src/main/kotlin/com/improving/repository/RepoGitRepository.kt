package com.improving.repository

import com.improving.domain.Repository
import org.springframework.data.jpa.repository.JpaRepository

interface RepoGitRepository: JpaRepository<Repository, Long>
