package com.improving.repository

import com.improving.domain.Milestone
import org.springframework.data.jpa.repository.JpaRepository

interface MilestoneRepository: JpaRepository<Milestone, Long>
