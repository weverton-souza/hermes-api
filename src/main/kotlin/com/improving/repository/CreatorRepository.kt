package com.improving.repository

import com.improving.domain.Creator
import org.springframework.data.jpa.repository.JpaRepository

interface CreatorRepository: JpaRepository<Creator, Long>
