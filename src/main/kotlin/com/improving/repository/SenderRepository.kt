package com.improving.repository

import com.improving.domain.Sender
import org.springframework.data.jpa.repository.JpaRepository

interface SenderRepository: JpaRepository<Sender, Long>
