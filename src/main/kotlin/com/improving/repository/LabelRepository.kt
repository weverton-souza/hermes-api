package com.improving.repository

import com.improving.domain.Label
import org.springframework.data.jpa.repository.JpaRepository

interface LabelRepository: JpaRepository<Label, Long>
