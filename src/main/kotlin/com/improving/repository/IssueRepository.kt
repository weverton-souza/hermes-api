package com.improving.repository

import com.improving.domain.Issue
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository

interface IssueRepository: JpaRepository<Issue, Long> {
    fun findByExternalId(issueId: Long): Optional<Issue>
}
