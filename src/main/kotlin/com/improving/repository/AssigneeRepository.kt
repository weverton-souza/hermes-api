package com.improving.repository

import com.improving.domain.Assignee
import org.springframework.data.jpa.repository.JpaRepository

interface AssigneeRepository: JpaRepository<Assignee, Long>
