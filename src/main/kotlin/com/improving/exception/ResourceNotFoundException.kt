package com.improving.exception

class ResourceNotFoundException(
        var developerMessage: String? = null,
        var status: Int? = null,
        override var message: String? = null
): RuntimeException()
