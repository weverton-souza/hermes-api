package com.improving.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class BusinessExceptionHandler : ResponseEntityExceptionHandler(){
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = [ResourceNotFoundException::class])
    fun resourceNotFound(ex: ResourceNotFoundException) =
            ResourceNotFoundException(
                    developerMessage = ex.developerMessage,
                    status = HttpStatus.NOT_FOUND.value(),
                    message = HttpStatus.NOT_FOUND.reasonPhrase
            )
}
