package com.improving

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
class HermesApiApplication

fun main(args: Array<String>) {
	runApplication<HermesApiApplication>(*args)
}
