package com.improving.service

import com.improving.domain.events.IssueEvent
import com.improving.facade.IssueEventFacade
import org.springframework.stereotype.Service

@Service
class IssueEventService(private val issueEventFacade: IssueEventFacade) {

    fun save(issueEvent: IssueEvent) = this.issueEventFacade.saveIssue(issueEvent)

    fun findEventByIssueExternalId(externalId: Long) = this.issueEventFacade.findEventByIssueExternalId(externalId)
}
